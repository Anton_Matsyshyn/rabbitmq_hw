﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;
using _RabbitMQ.Services;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;

namespace Pinger
{
    class Program
    {
        static IConfigurationRoot configuration;
        static void Main(string[] args)
        {
            configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory).Parent.Parent.Parent.FullName)
            .AddJsonFile("appsettings.json", false)
            .Build();

            var connectionSettings = new ConnectionSettings { Host = configuration.GetSection("ConnectionSettings:Host").Value };

            var producerScopeFactory = new ProducerScopeFactory(connectionSettings);
            var asProducer = new ScopeSettings
            {
                ExchangeName = "-PingPong-",
                ExchangeType = "direct",
                QueueName = "pong_queue",
                RoutingKey = "ping"
            };
            using IProducerScope producerScope = producerScopeFactory.CreateProducerScope(asProducer);

            var consumerScopeFactory = new ConsumerScopeFactory(connectionSettings);
            var asConsumer = new ScopeSettings
            {
                ExchangeName = "-PingPong-",
                ExchangeType = "direct",
                QueueName = "ping_queue",
                RoutingKey = "pong"
            };
            using IConsumerScope consumerScope = consumerScopeFactory.CreateConsumerScope(asConsumer);

            consumerScope.Consumer.Received += (o,e) =>
            {
                var message = Encoding.UTF8.GetString(e.Body.ToArray());
                Console.WriteLine($"{message} ({DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt")})");
                Thread.Sleep(2500);
                producerScope.Producer.Send("ping");
            };
            consumerScope.Consumer.Connect();

            producerScope.Producer.Send("ping");

            Console.WriteLine("Press [Enter] to stop");
            Console.ReadKey();
        }
    }
}
