﻿using _RabbitMQ.Models;

namespace _RabbitMQ.Interfaces
{
    public interface IProducerScopeFactory
    {
        IProducerScope CreateProducerScope(ScopeSettings scopeSettings);
    }
}
