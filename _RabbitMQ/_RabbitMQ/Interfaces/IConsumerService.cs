﻿using _RabbitMQ.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Interfaces
{
    public interface IConsumerService
    {
        ConsumerSettings ConsumerSettings { get; }
        void ListenQueue(EventHandler<BasicDeliverEventArgs> callBack);
    }
}
