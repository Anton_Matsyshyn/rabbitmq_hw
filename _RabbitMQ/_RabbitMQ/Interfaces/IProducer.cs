﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Interfaces
{
    public interface IProducer
    {
        void Send(string message, string type=null);
    }
}
