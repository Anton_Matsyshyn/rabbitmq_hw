﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Interfaces
{
    public interface IProducerScope:IDisposable
    {
        IProducer Producer { get; }
    }
}
