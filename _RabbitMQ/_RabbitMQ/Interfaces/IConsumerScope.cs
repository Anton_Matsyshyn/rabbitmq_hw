﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Interfaces
{
    public interface IConsumerScope : IDisposable
    {
        IConsumer Consumer { get;}
    }
}
