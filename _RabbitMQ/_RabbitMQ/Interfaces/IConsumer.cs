﻿using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Interfaces
{
    public interface IConsumer
    {
        event EventHandler<BasicDeliverEventArgs> Received;
        void Connect();
    }
}
