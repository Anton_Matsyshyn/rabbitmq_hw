﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Interfaces
{
    public interface IQueueService: IDisposable
    {
        IModel Channel { get; }
        void DeclareExchange(string exchange, string exchangeType);
        void BindQueue(string queue, string exchange, string routingKey);
    }
}
