﻿using _RabbitMQ.Models;
using _RabbitMQ.Services;

namespace _RabbitMQ.Interfaces
{
    public interface IConsumerScopeFactory
    {
        IConsumerScope CreateConsumerScope(ScopeSettings scopeSettings);
    }
}
