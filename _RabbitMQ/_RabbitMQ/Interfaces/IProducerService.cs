﻿using _RabbitMQ.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Interfaces
{
    public interface IProducerService
    {
        ProducerSettings ProducerSettings { get; }
        void SendMessageToQueue(string message);
    }
}
