﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Models
{
    public class ScopeSettings
    {
        public string ExchangeName { get; set; }
        public string QueueName { get; set; }
        public string RoutingKey { get; set; }
        public string ExchangeType { get; set; }
    }
}
