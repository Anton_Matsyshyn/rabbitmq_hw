﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Models
{
    public class ConsumerSettings
    {
        public bool SequrntialFetch { get; set; } = true;
        public bool AutoAcknowledge { get; set; } = true;
        public IModel Channel { get; set; }
        public string QueueName { get; set; }
    }
}
