﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Models
{
    public class ProducerSettings
    {
        public IModel Channel { get; set; }
        public PublicationAddress PublicationAddress { get; set; }
    }
}
