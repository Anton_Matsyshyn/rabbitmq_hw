﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;

namespace _RabbitMQ.Services
{
    public class ProducerScopeFactory:IProducerScopeFactory
    {
        private readonly ConnectionSettings _connectionSettings;

        public ProducerScopeFactory(ConnectionSettings connectionSettings)
        {
            _connectionSettings = connectionSettings;
        }

        public IProducerScope CreateProducerScope(ScopeSettings scopeSettings)
        {
            return new ProducerScope(_connectionSettings, scopeSettings);
        }
    }
}
