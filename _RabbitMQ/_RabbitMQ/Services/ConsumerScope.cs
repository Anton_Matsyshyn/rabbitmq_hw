﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Services
{
    public class ConsumerScope:IConsumerScope
    {
        private readonly Lazy<IQueueService> _queueServiceLazy;
        private readonly Lazy<IConsumer> _consumerLazy;

        private readonly ConnectionSettings _connectionSettings;
        private readonly ScopeSettings _scopeSettings;

        public ConsumerScope(ConnectionSettings connectionSettings,
                             ScopeSettings scopeSettings)
        {
            _connectionSettings = connectionSettings;
            _scopeSettings = scopeSettings;

            _queueServiceLazy = new Lazy<IQueueService>(CreateQueueService);
            _consumerLazy = new Lazy<IConsumer>(CreateConsumer);
        }
        public IConsumer Consumer => _consumerLazy.Value;
        private IQueueService QueueService => _queueServiceLazy.Value;

        private IQueueService CreateQueueService()
        {
            return new QueueService(_connectionSettings,
                                    _scopeSettings);
        }

        private IConsumer CreateConsumer()
        {
            return new Consumer(new ConsumerSettings
            {
                Channel = QueueService.Channel,
                QueueName = _scopeSettings.QueueName
            });
        }

        public void Dispose()
        {
            QueueService?.Dispose();
        }
    }
}
