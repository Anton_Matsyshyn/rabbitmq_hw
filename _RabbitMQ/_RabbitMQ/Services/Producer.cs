﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Services
{
    public class Producer : IProducer
    {
        private readonly ProducerSettings _settings;
        private readonly IBasicProperties _properties;
        public Producer(ProducerSettings settings)
        {
            _settings = settings;
            _properties = _settings.Channel.CreateBasicProperties();
            _properties.Persistent = true;
        }
        public void Send(string message, string type = null)
        {
            if(!string.IsNullOrEmpty(type))
            {
                _properties.Type = type;
            }

            var body = Encoding.UTF8.GetBytes(message);
            _settings.Channel.BasicPublish(_settings.PublicationAddress, null, body);
        }
    }
}
