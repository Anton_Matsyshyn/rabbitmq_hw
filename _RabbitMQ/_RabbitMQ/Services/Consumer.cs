﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace _RabbitMQ.Services
{
    public class Consumer : IConsumer
    {
        private readonly ConsumerSettings _settings;
        private readonly EventingBasicConsumer _consumer;

        public Consumer(ConsumerSettings settings)
        {
            _settings = settings;
            _consumer = new EventingBasicConsumer(_settings.Channel);
        }

        public event EventHandler<BasicDeliverEventArgs> Received
        {
            add => _consumer.Received += value;
            remove => _consumer.Received -= value;
        }

        public void Connect()
        {
            if (_settings.SequrntialFetch)
            {
                _settings.Channel.BasicQos(0, 1, false);
            }

            _settings.Channel.BasicConsume(_settings.QueueName, _settings.AutoAcknowledge, _consumer);
        }
    }
}
