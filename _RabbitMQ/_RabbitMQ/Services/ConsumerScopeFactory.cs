﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;

namespace _RabbitMQ.Services
{
    public class ConsumerScopeFactory:IConsumerScopeFactory
    {
        private readonly ConnectionSettings _connectionSettings;

        public ConsumerScopeFactory(ConnectionSettings connectionSettings)
        {
            _connectionSettings = connectionSettings;
        }

        public IConsumerScope CreateConsumerScope(ScopeSettings scopeSettings)
        {
            return new ConsumerScope(_connectionSettings, scopeSettings);
        }
    }
}
