﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;
using RabbitMQ.Client;
using System;

namespace _RabbitMQ.Services
{
    public class QueueService : IQueueService
    {
        readonly IConnection _connection;
        public QueueService(ConnectionSettings connectionSettins)
        {
            _connection = ConnectionFactory.CreateConnection(connectionSettins);
            Channel = _connection.CreateModel();
        }

        public QueueService(ConnectionSettings connectionSettings, ScopeSettings scopeSettings) : this(connectionSettings)
        {
            DeclareExchange(scopeSettings.ExchangeName, scopeSettings.ExchangeType);

            if(scopeSettings.QueueName != null)
            {
                BindQueue(scopeSettings.QueueName, scopeSettings.ExchangeName, scopeSettings.RoutingKey);
            }
        }
        public IModel Channel { get; }

        public void DeclareExchange(string exchange, string exchangeType)
        {
            Channel.ExchangeDeclare(exchange, exchangeType ?? "");
        }

        public void BindQueue(string queue, string exchange, string routingKey)
        {
            Channel.QueueDeclare(queue, true, false, false);
            Channel.QueueBind(queue, exchange, routingKey);
        }

        public void Dispose()
        {
            Channel.Dispose();
            _connection.Dispose();
        }
    }
}
