﻿using _RabbitMQ.Models;
using RabbitMQ.Client;
using System;
using System.IO;
using System.Reflection;

namespace _RabbitMQ.Services
{
    internal static class ConnectionFactory
    {
        public static IConnection CreateConnection(ConnectionSettings settings)
        {
            return new RabbitMQ.Client.ConnectionFactory 
            { 
                HostName = settings.Host
                //etc
            }.CreateConnection();
        }
    }
}
