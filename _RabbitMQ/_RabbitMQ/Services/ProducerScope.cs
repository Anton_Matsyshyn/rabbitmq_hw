﻿using _RabbitMQ.Interfaces;
using _RabbitMQ.Models;
using RabbitMQ.Client;
using System;

namespace _RabbitMQ.Services
{
    public class ProducerScope:IProducerScope
    {
        private readonly Lazy<IQueueService> _queueServiceLazy;
        private readonly Lazy<IProducer> _producerLazy;

        private readonly ScopeSettings _scopeSettings;
        private readonly ConnectionSettings _connectionSettings;
        public ProducerScope(ConnectionSettings connectionSettings,
                             ScopeSettings scopeSettings)
        {
            _connectionSettings = connectionSettings;
            _scopeSettings = scopeSettings;

            _queueServiceLazy = new Lazy<IQueueService>(CreateQueueService);
            _producerLazy = new Lazy<IProducer>(CreateProducer);
        }

        public IProducer Producer => _producerLazy.Value;
        private IQueueService QueueService => _queueServiceLazy.Value;

        private IQueueService CreateQueueService()
        {
            return new QueueService(_connectionSettings,
                                    _scopeSettings);
        }

        private IProducer CreateProducer()
        {
            return new Producer(new ProducerSettings
            {
                Channel = QueueService.Channel,
                PublicationAddress = new PublicationAddress(
                    _scopeSettings.ExchangeType,
                    _scopeSettings.ExchangeName,
                    _scopeSettings.RoutingKey)
            });
        }

        public void Dispose()
        {
            QueueService?.Dispose();
        }
    }
}
